/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#include "error_code.hpp"
#include "ll_http_client.hpp"
#include "http_request.hpp"
#include "http_response.hpp"
#include "response_reader.hpp"

namespace low_level
{

HTTPClient::HTTPClient(boost::asio::io_service& io_svc)
    : io_svc_(io_svc),
      resolver_(io_svc),
      connector_(io_svc),
      conn_cache_(io_svc),
      reader_(new HTTPDataReader<HTTPResponse>(io_svc_))
{
  //std::cout << __FUNCTION__ << std::endl;
}

void HTTPClient::send_request(HTTPRequest& request,
    ResponseHandler handler)
{
  // we can only work from the io_svc_ context!
//  std::cout << __FUNCTION__ << std::endl;

  RequestContext::ContextPtr ctx( new RequestContext(this, request, handler) );
  ctx->send_request(ctx);
}

void HTTPClient::RequestContext::send_request(ContextPtr ctx)
{
//  std::cout << __FUNCTION__ << std::endl;

  ConnectionPtr conn( client->conn_cache_.get_connection(
      request.get_url().get_host(), request.get_url().get_port()) );
  if (conn)
  {
    on_connection(boost::system::error_code(), conn, ctx);
    return;
  }

  client->resolver_.async_resolve(request.get_url().get_host(),
      request.get_url().get_port(),
      boost::bind(&HTTPClient::RequestContext::on_resolve, this, _1, _2, ctx));
}

void HTTPClient::RequestContext::on_resolve(const boost::system::error_code& error,
    boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
    ContextPtr ctx)
{
//  std::cout << __FUNCTION__ << std::endl;
  if (error)
  {
    handler(error, ResponsePtr());
    return;
  }
  client->connector_.async_connect(endpoint_iterator, request.get_url().get_protocol(),
      boost::bind(&HTTPClient::RequestContext::on_connection, this, _1, _2, ctx));
}

void HTTPClient::RequestContext::on_connection(const boost::system::error_code& error,
    ConnectionPtr connection,
    ContextPtr ctx)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str())
//      << std::endl;
  if (error)
  {
    handler(error, ResponsePtr());
    return;
  }
  assert(connection);
  this->connection = connection;

  request.to_data_stream().swap(this->buf);
  connection->async_write(*buf,
      boost::bind(&HTTPClient::RequestContext::on_request_sent, this, _1, _2, ctx));
}

void HTTPClient::RequestContext::on_request_sent(const boost::system::error_code& error,
    size_t bytes, ContextPtr ctx)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str())
//      << ", bytes: " << bytes << std::endl;
  if (error)
  {
    handler(error, ResponsePtr());
    return;
  }

  // wait for on_message_received
  client->reader_->async_read<HTTPClient::RequestContext>(connection, ctx);
}

void HTTPClient::RequestContext::on_message_received(const boost::system::error_code& error,
    ResponsePtr response,
    ContextPtr ctx)
{
  if (response && response->is_connection_keep_alive())
  {
    client->conn_cache_.store(request.get_url().get_host(), request.get_url().get_port(), connection);
  }
  handler(error, response);
}

} // namespace low_level
