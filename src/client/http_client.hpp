/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_HTTP_CLIENT_HPP
#define HTTP_TOY_HTTP_CLIENT_HPP

#include "ll_http_client.hpp"

class HTTPClient
{
public:
  HTTPClient(boost::asio::io_service& io_svc);

  typedef low_level::HTTPClient::RequestPtr RequestPtr;
  typedef low_level::HTTPClient::ResponsePtr ResponsePtr;
  typedef low_level::HTTPClient::ResponseHandler ResponseHandler;

  void send_request(RequestPtr& request,
      ResponseHandler handler);

private:

  struct ExtraInfo
  {
    ExtraInfo(int retry_count, int redirects_count)
        : retry_count(retry_count)
        , redirects_count(redirects_count)
        {}

    int retry_count;
    int redirects_count;
  };

  void send_request_impl(RequestPtr& request,
      ResponseHandler handler,
      HTTPClient::ExtraInfo info);

  void on_response(const boost::system::error_code& error,
      RequestPtr request, ResponsePtr response, ResponseHandler handler,
      HTTPClient::ExtraInfo info);

  /*
  void on_decompress(const boost::system::error_code& error,
      RequestPtr request, ResponsePtr response);
  */

  boost::asio::io_service& io_svc_;
  low_level::HTTPClient ll_client_;
  int retry_count_;
  int redirects_count_;
};


#endif // HTTP_TOY_HTTP_CLIENT_HPP
