/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_DATA_BLOCK_HPP
#define HTTP_TOY_DATA_BLOCK_HPP

#include <istream>
#include <map>
#include <string>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/asio.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

#include "text_util.hpp"

class DataBlock
{
public:
  DataBlock()
  {
  }

  typedef std::vector<char> content_buf_type;
  const content_buf_type& get_content() const
  {
    return content_;
  }

  template<typename Iterator>
  void add_content(Iterator begin, Iterator end)
  {
    std::size_t prev_size = content_.size();
    content_.resize(prev_size + std::distance(begin, end));
    std::copy(begin, end, content_.begin() + prev_size);
  }

  std::pair<bool, std::size_t> get_content_length() const
  {
    static const std::string conn_len_name("Content-Length");
    const std::pair<bool, std::string>& length = get_header(conn_len_name);
    return std::make_pair(length.first,
        (length.first ? boost::lexical_cast<std::size_t>(length.second) : 0));
  }

  std::size_t get_received_data_length() const
  {
    return content_.size();
  }

  bool is_connection_keep_alive() const
  {
    static const std::string connection_name("Connection");
    const std::pair<bool, std::string>& connection = get_header(connection_name);
    if (connection.first)
    {
      static const std::string keep_alive("Keep-Alive");
      return text::ifind(connection.second, keep_alive) || text::ifind(connection.second, keep_alive);
    }

    static const std::string http_1_0("HTTP/1.0");
    return !boost::algorithm::iequals(http_version_, http_1_0);
  }

  bool is_chunked() const
  {
    static const std::string transfer_encoding("Transfer-Encoding");
    const std::pair<bool, std::string>& encoding = get_header(transfer_encoding);

    static const std::string chunked("chunked");
    return encoding.first && text::ifind(encoding.second, chunked);
  }

  bool is_gzipped() const
  {
    const std::pair<bool, std::string>& encoding = get_header("Content-Encoding");
    return encoding.first && text::ifind(encoding.second, "gzip");

  }

  void add_header(const std::string& key, const std::string& value)
  {
    headers_.insert(std::make_pair(key, value));
  }

  void add_header(boost::system::error_code& ec, const std::string& key_value)
  {
    ec = boost::system::error_code();
    std::size_t colon = key_value.find(':');
    if (colon == std::string::npos || colon == 0 || (colon == key_value.size()-1))
    {
      ec = http_client::parsing_error;
      return;
    }
    add_header(
        std::string(key_value.begin(), key_value.begin()+colon),
        std::string(key_value.begin()+colon+1, key_value.end()));
  }

  std::pair<bool, std::string> get_header(const std::string& key) const
  {
    headers_map::const_iterator it = headers_.find(key);
    if (it != headers_.end())
    {
      return std::make_pair(true, it->second);
    }
    return std::make_pair(false, std::string());
  }

  static void parse_header(
      boost::system::error_code& ec,
      DataBlock& data,
      boost::asio::streambuf& response_buf)
  {
    ec = boost::system::error_code();
    std::istream iss(&response_buf);

    static const std::string CR("\r");
    std::string header;
    while (std::getline(iss, header) && header != CR)
    {
      std::size_t separator = header.find(':');
      if (separator == std::string::npos)
      {
        ec = http_client::parsing_error;
        return;
      }

      std::string key(header.begin(), header.begin() + separator);
      std::string value(header.begin() + separator + 2, header.end() - 1);
      data.headers_.insert(std::make_pair(key, value));
    }

    if (data.is_chunked())
    {
      return;
    }
    else
    {
      boost::asio::streambuf::const_buffers_type content_buf =
          response_buf.data();
      data.content_.assign(boost::asio::buffers_begin(content_buf),
          boost::asio::buffers_end(content_buf));
    }
  }

protected:
  std::string http_version_;
  typedef std::multimap<std::string, std::string, text::icase_less> headers_map;
  headers_map headers_;
  content_buf_type content_;
};

#endif // HTTP_TOY_DATA_BLOCK_HPP
