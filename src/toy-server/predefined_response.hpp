/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_PREDEFINDED_RESPONSE_HPP
#define HTTP_TOY_PREDEFINDED_RESPONSE_HPP

#include <string>
#include <vector>

#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include "http_connection.hpp"

class HTTPRequest;
class HTTPResponse;
template<typename MessageType>
class HTTPDataReader;

class PredefinedResponse
{
public:

  typedef boost::shared_ptr<HTTPResponse> ResponsePtr;

  static const ResponsePtr& not_found()
  {
    static boost::shared_ptr<HTTPResponse> response;
    if (!response)
    {
      response.reset(new HTTPResponse());
      response->set_code(404);
      response->set_status("Not found");
      std::string msg("<title>HTTP-Toy Server</title><body>");
      for (int i = 0; i<5; ++i)
      {
        msg += "<h1>There should be some text</h1><br/>";
      }
      for (int i = 0; i<10; ++i)
      {
        msg += "<h1>Nothing to see here</h1><br/>";
      }
      msg += "</body>\r\n";
      response->add_content(msg.begin(), msg.end());
    }
    return response;
  }

  static const ResponsePtr& internal_error()
  {
    static ResponsePtr internal_server_error;
    if (!internal_server_error)
    {
      internal_server_error = boost::make_shared<HTTPResponse>();
      internal_server_error->set_code(500);
      internal_server_error->set_status("Internal HTTP-Toy Server Error");
      std::string msg("<title>HTTP-Toy Server</title><body><h1>Internal HTTP-Toy Server Error</h1></body>\r\n");
      internal_server_error->add_content(msg.begin(), msg.end());
    }
    return internal_server_error;
  }

  static const ResponsePtr& forbidden()
  {
    static ResponsePtr response;
    if (!response)
    {
      response = boost::make_shared<HTTPResponse>();
      response->set_code(403);
      response->set_status("Forbidden");
      std::string msg("<title>HTTP-Toy Server</title><body>");
      for (int i = 0; i<10; ++i)
      {
        msg += "<h1>Nothing to see here</h1><br/>";
      }
      msg += "</body>\r\n";
      response->add_content(msg.begin(), msg.end());
    }
    return response;
  }

};

#endif // HTTP_TOY_PREDEFINDED_RESPONSE_HPP
