/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_RESPONSE_READER_HPP
#define HTTP_TOY_RESPONSE_READER_HPP

#include <boost/function.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/shared_ptr.hpp>

class HTTPResponse;

template<typename MessageType>
class ChunkedDataReader
{
  struct Context
  {
    static const int no_size = -1;
    Context()
    : last_chunk_size_(no_size)
    , last_chunk_read_(0)
    {}

    int last_chunk_size_;
    int last_chunk_read_;
  };

public:

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;
  typedef boost::shared_ptr<MessageType> MessagePtr;
  typedef boost::function<void(const boost::system::error_code&, MessagePtr)> Handler;

  void async_read(ConnectionPtr connection,
      const MessagePtr& message,
      boost::asio::streambuf& message_buf,
      Handler handler)
  {
    Context ctx;
    read_buf(boost::system::error_code(), ctx, "", connection, message, message_buf, handler);
  }

private:

  void read_buf(
      const boost::system::error_code& error,
      Context ctx,
      std::string chunk_size_piece,
      const ConnectionPtr& connection,
      const MessagePtr& message,
      boost::asio::streambuf& message_buf,
      Handler handler)
  {
    //std::cout << __FUNCTION__ << std::endl;

    if (error && error != boost::asio::error::eof)
    {
      handler(error, message);
      return;
    }

    typedef boost::asio::streambuf::const_buffers_type buf_type;
    typedef boost::asio::buffers_iterator<buf_type> iterator;

    while (true)
    {
      //std::cout << __FUNCTION__ << " while" << ctx.last_chunk_size_ << " " << ctx.last_chunk_read_ << std::endl;
      if (ctx.last_chunk_size_ == Context::no_size)
      {
        std::istream iss(&message_buf);

        std::string chunk_size;
        // NOTE: eof means that we did not meet \n
        if (!std::getline(iss, chunk_size) || iss.eof())
        {
          truncate_cr(chunk_size);
          // TODO: read [at least] full chunk size till \n
          connection->async_read_until(message_buf, "\n",
              boost::bind(&ChunkedDataReader::read_buf, this,
                  boost::asio::placeholders::error,
                  ctx, chunk_size_piece + chunk_size, connection, message, boost::ref(message_buf), handler));
          return;
        }

        truncate_cr(chunk_size);
        std::copy(chunk_size.begin(), chunk_size.end(),
            std::back_inserter(chunk_size_piece));
        chunk_size_piece.swap(chunk_size);
        chunk_size_piece.clear();

        if (!chunk_size.size())
        {
          chunk_size_piece.swap(chunk_size);
          continue;
        }


        char * p = 0;
        ctx.last_chunk_size_ = strtol( chunk_size.c_str(), &p, 16 );

        //std::cout << "chunk size: '" << chunk_size << "' " << ctx.last_chunk_size_ << std::endl;

        if (ctx.last_chunk_size_ == 0)
        {
          // response is fully received
          // even if we do not have the last \r\n, it does not matter now
          handler(boost::system::error_code(), message);
          return;
        }

      }
      else
      {
        buf_type content_buf = message_buf.data();
        iterator begin = boost::asio::buffers_begin(content_buf);
        iterator end = boost::asio::buffers_end(content_buf);
        std::size_t bytes_to_write = std::min<int>(
            ctx.last_chunk_size_ - ctx.last_chunk_read_,
            (end - begin));
            //std::distance(begin, end));

        message->add_content(begin, begin + bytes_to_write);

        ctx.last_chunk_read_ += bytes_to_write;
        message_buf.consume(bytes_to_write);
//        std::cout << "bytes read: " << ctx.last_chunk_read_ << " from " << ctx.last_chunk_size_ << std::endl;

        if (ctx.last_chunk_read_ == ctx.last_chunk_size_)
        {
          ctx.last_chunk_read_ = 0;
          ctx.last_chunk_size_ = Context::no_size;
        }
        else
        {
          // read data block
          connection->async_read(message_buf, ctx.last_chunk_size_ - ctx.last_chunk_read_ + 2,
              boost::bind(&ChunkedDataReader::read_buf, this,
                  boost::asio::placeholders::error,
                  ctx, "", connection, message, boost::ref(message_buf), handler));
          return;
        }
      }
    } // while (true)
  } // read_buf

  void truncate_cr(std::string & buf)
  {
    if (buf.size())
    {
      if (buf[buf.size()-1] == '\r')
      {
        buf.resize(buf.size()-1);
      }
    }
  }
}; // ChunkedResponseReader


template<typename MessageType>
class HTTPDataReader
{
public:

  HTTPDataReader(boost::asio::io_service& io_svc, bool read_unknown_stuff = true)
  : io_svc_(io_svc)
  , risk_reading_(read_unknown_stuff)
  {}

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;
  typedef boost::shared_ptr<MessageType> MessagePtr;
  //typedef boost::function< void(const boost::system::error_code&, MessagePtr) > Handler;

  template<typename Handler>
  struct ReaderContext
  {
    HTTPDataReader * reader;
    ConnectionPtr connection;
    boost::shared_ptr<Handler> handler;
    boost::asio::streambuf message_buf;
    std::vector<char> buf;
    MessagePtr message;
    typedef ReaderContext<Handler> ReaderContextType;
    typedef boost::shared_ptr<ReaderContextType> ReaderContextPtr;

    void on_header_received(
        const boost::system::error_code& error, size_t bytes,
        const ReaderContextPtr& ctx)
    {
//      std::cout << __FUNCTION__ << " "
//          << (!error ? "successfully" :
//                       (std::string("with error(") + error.message() + ")").c_str())
//          << ", bytes: " << bytes << std::endl;
      if (error)
      {
        handler->on_message_received(error, MessagePtr(), handler);
        return;
      }

      message = MessageType::parse_header(message_buf);
      if (!message)
      {
        handler->on_message_received(http_client::parsing_error, MessagePtr(), handler);
        return;
      }

      if (message->is_chunked())
      {
        //std::cout << "connection seems to be 'chunked'" << std::endl;
        reader->chunked_reader_.async_read(connection, message, message_buf,
            boost::bind(&ReaderContextType::on_chunked_body_received, this,
                _1, _2, ctx));
        return;
      }

      std::pair<bool, std::size_t> length_in_header = message->get_content_length();
      if (length_in_header.first)
      {
        std::size_t bytes_to_add = length_in_header.second
            - message->get_received_data_length();
        if (bytes_to_add)
        {
          buf.resize(bytes_to_add);
          connection->async_read(buf,
              boost::bind(&ReaderContextType::on_sized_body_received, this, _1, _2,
                  ctx));
        }
        else
        {
          // body is received
          handler->on_message_received(boost::system::error_code(), message, handler);
          return;
        }
      }
      else if (!message->is_connection_keep_alive())
      {
        //std::cout << "connection seems to be 'close'" << std::endl;
        if (reader->risk_reading_)
        {
          buf.resize(4096);
          connection->async_read_until_eof(buf,
              boost::bind(&ReaderContextType::on_sized_body_received, this, _1, _2,
                  ctx));
        }
        else
        {
          handler->on_message_received(boost::system::error_code(), message, handler);
        }
      }
      else
      {
        // missing content length and incorrect connection type
        handler->on_message_received(
            reader->risk_reading_ ? http_client::cannot_finish_connection : boost::system::error_code(),
                message, handler);
      }
    }

    void on_chunked_body_received(
        const boost::system::error_code& error,
        const MessagePtr& message,
        const boost::shared_ptr<ReaderContext<Handler> >& ctx)
    {
      handler->on_message_received(error, message, handler);
    }

    void on_sized_body_received(
        const boost::system::error_code& error, size_t bytes,
        const boost::shared_ptr<ReaderContext<Handler> >& ctx)
    {
      if (ctx)
      {
        message->add_content(buf.begin(), buf.end());
      }
      handler->on_message_received(error, message, handler);
    }
  }; // ReaderContext

  template<typename Handler>
  void async_read(
      const ConnectionPtr& connection,
      const boost::shared_ptr<Handler>& handler)
  {
    boost::shared_ptr<ReaderContext<Handler> > ctx( boost::make_shared<ReaderContext<Handler> >());
    ctx->reader = this;
    ctx->connection = connection;
    ctx->handler = handler;

    const static std::string CRLFCRLF("\r\n\r\n");
    connection->async_read_until(ctx->message_buf, CRLFCRLF,
        boost::bind(&ReaderContext<Handler>::on_header_received, ctx.get(), _1, _2,
            ctx));
  }

private:

  boost::asio::io_service& io_svc_;
  ChunkedDataReader<MessageType> chunked_reader_;
  bool risk_reading_;

}; // class HTTPDataReader


#endif // HTTP_TOY_RESPONSE_READER_HPP
