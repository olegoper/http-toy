/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include <boost/bind.hpp>

#include "http_client.hpp"
#include "http_request.hpp"
#include "http_response.hpp"
#include "error_code.hpp"
#include "url.hpp"


HTTPClient::HTTPClient(boost::asio::io_service& io_svc)
    : io_svc_(io_svc)
    , ll_client_(io_svc)
    , retry_count_(3)
    , redirects_count_(3)
{
  //std::cout << __FUNCTION__ << std::endl;
}

void HTTPClient::send_request(RequestPtr& request,
    ResponseHandler handler)
{
//  std::cout << __FUNCTION__ << std::endl;
  ExtraInfo info(retry_count_, redirects_count_);
  io_svc_.post( boost::bind(&HTTPClient::send_request_impl, this, request, handler, info) );
}

void HTTPClient::send_request_impl(RequestPtr& request,
    ResponseHandler handler, ExtraInfo info)
{
//  std::cout << __FUNCTION__ << std::endl;
  ll_client_.send_request(*request,
      boost::bind(&HTTPClient::on_response, this, _1, request, _2, handler, info));
}

void HTTPClient::on_response(
    const boost::system::error_code& error,
    RequestPtr request,
    ResponsePtr http_response,
    ResponseHandler handler,
    ExtraInfo info)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str()) << std::endl;

  if (error)
  {
    if (!info.retry_count)
    {
      handler(error, http_response);
      return;
    }
    else
    {
      --info.retry_count;
      send_request_impl(request, handler, info);
      return;
    }
  }

  int code = http_response->get_code();
//  std::cout << "response code: " << code << std::endl;
  if (301 <= code && code <= 303 && info.redirects_count)
  {
    std::pair<bool, std::string> location(http_response->get_header("Location"));
    if (location.first)
    {
      boost::system::error_code parsing_error;
      URL new_url;
      URL::parse_url(parsing_error, new_url, location.second, request->get_url().get_host());
      if (!parsing_error)
      {
        request->redirect(new_url);
        --info.redirects_count;
        send_request_impl(request, handler, info);
        return;
      }
    }

    // Let the caller handle this strange redirect to somewhere else
    // with ill-formed of missing new url in "Location" header
  }

  // TODO: good candidate to be performed asynchronously
  if (http_response->is_gzipped())
  {
    http_response->ungzip();
  }

  handler(error, http_response);
}
