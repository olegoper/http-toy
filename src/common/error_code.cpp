/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include "error_code.hpp"

namespace http_client
{


const char* my_error_category_impl::name() const
{
  return "my_errors";
}
std::string my_error_category_impl::message(int e) const
{
  switch (e)
  {
  case unsupported_protocol:
    return "unsupported protocol";
    break;
  case parsing_error:
    return "header_parsing_error";
    break;
  case cannot_finish_connection:
    return "cannot_finish_connection";
    break;
  default:
    break;
  }
  return "something wrong";
}


const boost::system::error_category& get_my_error_category()
{
  static my_error_category_impl cat;
  return cat;
}

boost::system::error_code make_error_code(my_errors e)
{
  return boost::system::error_code(static_cast<int>(e), get_my_error_category());
}

} // namespace http_client
