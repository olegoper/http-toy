/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include "http_server.hpp"
#include "http_request.hpp"
#include "http_response.hpp"
#include "response_reader.hpp"

HTTPServer::HTTPServer(boost::asio::io_service& io_svc)
: io_svc_(io_svc)
, acceptor_(io_svc_)
{}

void HTTPServer::start_server(const std::string& protocol, unsigned int port, RequestProcessor processor)

{
  // we should work in the context of our io_svc_
  boost::shared_ptr<ServerContext> srv_ctx( new ServerContext(io_svc_, this,
      protocol, port, processor) );
  io_svc_.post(boost::bind(&HTTPServer::start_server_impl, this, srv_ctx));
}

void HTTPServer::start_server_impl(boost::shared_ptr<HTTPServer::ServerContext> srv_ctx)
{
  // wait for on_accepted
  acceptor_.async_accept<HTTPServer::ServerContext>(srv_ctx->protocol, srv_ctx->port, srv_ctx);
}


HTTPServer::ServerContext::ServerContext(boost::asio::io_service& io_svc,
    HTTPServer * srv, const std::string& protocol,
    unsigned int port,
    RequestProcessor processor)
  : srv(srv)
  , io_svc_(io_svc)
  , protocol(protocol)
  , port(port)
  , processor(processor)
{
  reader_.reset(new HTTPDataReader<HTTPRequest>(io_svc_, false));
  std::cout << __FUNCTION__ << std::endl;
}

HTTPServer::ServerContext::~ServerContext()
{
  std::cout << __FUNCTION__ << std::endl;
}

void HTTPServer::ServerContext::on_accepted(const boost::system::error_code& error,
  HTTPServer::ConnectionPtr connection,
  boost::shared_ptr<HTTPServer::ServerContext> ctx)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str())
//      << std::endl;

  if (error)
  {
    return;
  }

  boost::shared_ptr<ClientContext> cln_ctx( boost::make_shared<ClientContext>() );
  cln_ctx->ctx = ctx;
  cln_ctx->connection = connection;

  // wait for on_message_received
  reader_->async_read<HTTPServer::ClientContext>(connection, cln_ctx);
}

void HTTPServer::ClientContext::on_message_received(
    const boost::system::error_code& error,
    RequestPtr request,
    boost::shared_ptr<ClientContext> cln_ctx)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str())
//      << std::endl;

  this->request = request;


  static ResponsePtr internal_server_error;
  if (!internal_server_error)
  {
    internal_server_error = boost::make_shared<HTTPResponse>();
    internal_server_error->set_code(500);
    internal_server_error->set_status("Internal HTTP-Toy Server Error");
    std::string msg("<title>HTTP-Toy Server</title><body><h1>}{peH BaM</h1></body>\r\n");
    internal_server_error->add_content(msg.begin(), msg.end());
  }

  if (error)
  {
//    if (error == boost::asio::error::eof)
//    {
//      ctx->srv->reader_->async_read<HTTPServer::ClientContext>(connection, cln_ctx);
//    }
    return;
  }

  //boost::shared_ptr<std::vector<char> > text = request->to_data_stream();
  //std::cout << "request: " << std::endl << std::string(text->begin(), text->end()) << " --- " << std::endl;

  if (error)
  {
    response = internal_server_error;
  }
  else
  {
    boost::system::error_code response_error;
    response = ctx->processor(response_error, *request);
    if (response_error)
    {
      response = internal_server_error;
    }
  }

  const static std::string connection_name("Connection");
  const static std::string keep_alive("Keep-Alive");
  if (request->is_connection_keep_alive() &&
      !response->get_header(connection_name).first)
  {
    response->add_header(connection_name, keep_alive);
  }

  response->to_data_stream( response_buf );

  //std::cout << "response: " << std::endl << std::string(response_buf.begin(), response_buf.end()) << " --- " << std::endl;
  connection->async_write(response_buf,
      boost::bind(&HTTPServer::ClientContext::on_response_sent, cln_ctx.get(),
          _1, _2,  cln_ctx));
}

void HTTPServer::ClientContext::on_response_sent(
    const boost::system::error_code& error, size_t bytes ,
    boost::shared_ptr<ClientContext> cln_ctx)
{
//  std::cout << __FUNCTION__ << " "
//      << (!error ? "successfully" :
//                   (std::string("with error(") + error.message() + ")").c_str())
//      << std::endl;

  if (error)
  {
    return;
  }

  if (this->request->is_connection_keep_alive() && this->response->is_connection_keep_alive())
  {
    // wait for on_message_received
    ctx->reader_->async_read<HTTPServer::ClientContext>(connection, cln_ctx);
  }
}
