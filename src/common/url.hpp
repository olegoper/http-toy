/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_URL_HPP
#define HTTP_TOY_URL_HPP

#include <string>
#include <boost/lexical_cast.hpp>

#include "error_code.hpp"

class URL
{
public:

  static void parse_url(boost::system::error_code& ec, URL& new_url, const std::string& url, const std::string& default_host = "localhost")
  {
//    std::cout << __FUNCTION__ << std::endl;

    bool protocol_found = false;
    std::string protocol("http");
    unsigned int port = 80;

    const std::string protocol_delimiter("://");
    std::size_t idx = url.find(protocol_delimiter);
    if (idx != std::string::npos)
    {
      protocol.assign(url.begin(), url.begin()+idx);
      idx += protocol_delimiter.size();
      if (protocol == "https")
      {
        port = 443;
      }
      protocol_found = true;
    }
    else
    {
      idx = 0;
    }

    std::size_t host_idx = url.find("/", idx);
    if (host_idx == std::string::npos)
    {
      host_idx = url.size();
    }

    std::string host(url.begin() + idx, url.begin() + host_idx);
    std::size_t port_idx = host.rfind(":");
    if (port_idx != std::string::npos)
    {
      port = boost::lexical_cast<unsigned int>(std::string(host.begin()+port_idx + 1, host.end()));
      host.resize(port_idx);
      if (!protocol_found)
      {
        switch (port)
        {
        case 80:
          protocol = "http";
          break;
        case 443:
          protocol = "https";
          break;
        default:
          break;
        }
      }
    }
    if (host.empty())
    {
      host.assign(default_host);
    }

    std::string path( url.begin() + host_idx, url.end() );
    if (path.empty())
    {
      path.assign("/");
    }

//    std::cout << protocol << " " << host << " " <<  port << " " << path << std::endl;
    //URL new_url(protocol, host, port, path);
    ec = boost::system::error_code();
    new_url.protocol_.swap(protocol);
    new_url.host_.swap(host);
    new_url.port_ = port;
    new_url.path_.swap(path);
  }

  URL(const std::string& protocol, const std::string& host, unsigned int port,
      const std::string& path)
      : protocol_(protocol),
        host_(host),
        port_(port),
        path_(path)
  {
  }

  URL()
      : protocol_("http"),
        host_("localhost"),
        port_(80),
        path_("/")
  {
  }

  const std::string& get_protocol() const
  {
    return protocol_;
  }

  const std::string& get_host() const
  {
    return host_;
  }

  unsigned int get_port() const
  {
    return port_;
  }

  const std::string& get_path() const
  {
    return path_;
  }

  void add_query(const std::string& key, const std::string& value)
  {
    std::stringstream oss;
    oss << ( query_.empty() ? "?" : "&" );
    oss << text::encode_url_query(key) << "=" << text::encode_url_query(value);
    query_.append(std::istreambuf_iterator<char>(oss), std::istreambuf_iterator<char>());
  }

  void add_query(boost::system::error_code& ec, const std::string& query_kv)
  {
    ec = boost::system::error_code();
    std::size_t delimiter = query_kv.find('=');
    if (delimiter == std::string::npos || delimiter == 0 || (delimiter == query_kv.size()-1))
    {
      ec = http_client::parsing_error;
      return;
    }

    add_query(
      std::string(query_kv.begin(), query_kv.begin()+delimiter),
      std::string(query_kv.begin()+delimiter+1, query_kv.end()));
  }

  const std::string& get_query() const
  {
    return query_;
  }

private:

  std::string protocol_;
  std::string host_;
  unsigned int port_;
  std::string path_;
  std::string query_;
};

#endif // HTTP_TOY_URL_HPP
