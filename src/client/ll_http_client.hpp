/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_LL_HTTP_CLIENT_HPP
#define HTTP_TOY_LL_HTTP_CLIENT_HPP

#include <boost/asio/io_service.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include "caching_resolver.hpp"
#include "connection_cache.hpp"
#include "http_connection.hpp"

class HTTPRequest;
class HTTPResponse;
template<typename MessageType>
class HTTPDataReader;

namespace low_level
{

class HTTPClient
{
public:
  HTTPClient(boost::asio::io_service& io_svc);

  typedef boost::shared_ptr<HTTPRequest> RequestPtr;
  typedef boost::shared_ptr<HTTPResponse> ResponsePtr;
  typedef boost::function<void(const boost::system::error_code&, ResponsePtr)> ResponseHandler;

  void send_request(HTTPRequest& request,
      ResponseHandler handler);

private:

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;

  class RequestContext
  {
  public:
    RequestContext(HTTPClient * client,
        HTTPRequest& request,
        ResponseHandler handler)
    : client(client)
    , request(request)
    , handler(handler)
    {}

    typedef boost::shared_ptr<HTTPClient::RequestContext> ContextPtr;

    void send_request(ContextPtr ctx);

    // called back by HTTPDataReader
    void on_message_received(const boost::system::error_code& error,
        ResponsePtr response, ContextPtr ctx);

  private:
    void on_resolve(const boost::system::error_code& error,
        boost::asio::ip::tcp::resolver::iterator endpoint_iterator, ContextPtr ctx);

    void on_connection(const boost::system::error_code& error,
        ConnectionPtr connection, ContextPtr ctx);

    void on_request_sent(const boost::system::error_code& error, size_t bytes,
        ContextPtr ctx);

    HTTPClient * client;
    HTTPRequest& request;
    ResponseHandler handler;

    ConnectionPtr connection;
    boost::shared_ptr<std::vector<char> > buf;
  };

  boost::asio::io_service& io_svc_;
  CachingResolver resolver_;
  HTTPConnector connector_;
  ConnectionCache conn_cache_;
  boost::shared_ptr< HTTPDataReader<HTTPResponse> > reader_;
}; // class HTTPClient

} // namespace low_level

#endif // HTTP_TOY_LL_HTTP_CLIENT_HPP
