/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_CONNECTION_CACHE_HPP
#define HTTP_TOY_CONNECTION_CACHE_HPP

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

class HTTPConnection;

class ConnectionCache
{
public:
  ConnectionCache(boost::asio::io_service& io_svc)
      : io_svc_(io_svc)
  {
  }

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;

  ConnectionPtr get_connection(const std::string& host, unsigned int port)
  {
//    std::cout << __FUNCTION__ << std::endl;
    cache_key_type key( std::make_pair(host, port) );
    connections_cache_type::iterator connection = connections_.find(key);
    if (connection != connections_.end())
    {
//      std::cout << "getting from cache: " << host << ":" << port << std::endl;
      ConnectionPtr result = connection->second;
      connections_.erase(connection);
      return result;
    }

    return ConnectionPtr();
  }

  void store(const std::string& host, unsigned int port, ConnectionPtr connection)
  {
//    std::cout << __FUNCTION__ << std::endl;
    cache_key_type key( std::make_pair(host, port) );

    connections_.insert( std::make_pair(key, connection) );
  }

private:
  boost::asio::io_service& io_svc_;

  typedef std::pair<std::string, unsigned int> cache_key_type;
  typedef std::multimap<cache_key_type, ConnectionPtr> connections_cache_type;
  connections_cache_type connections_;
};

#endif // HTTP_TOY_CONNECTION_CACHE_HPP
