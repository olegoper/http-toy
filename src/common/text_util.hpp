/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_TEXT_UTIL_HPP
#define HTTP_TOY_TEXT_UTIL_HPP

#include <set>
#include <string>

namespace text
{

std::string encode_base64(const std::string& data);

std::string encode_url_path(const std::string& data);
std::string encode_url_query(const std::string& data);
std::string encode_url(const std::string& data, const std::set<char>& allowed);

// case insensitive search check
bool ifind(const std::string& source, const std::string& target);

// case insensitive comparator
struct icase_less : public std::binary_function<std::string, std::string, bool>
{
  bool operator()(const std::string &lhs, const std::string &rhs) const;
};

} // namespace text


#endif // HTTP_TOY_TEXT_UTIL_HPP
