/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_ERROR_CODE_HPP
#define HTTP_TOY_ERROR_CODE_HPP

#include <string>

#include <boost/system/error_code.hpp>

namespace http_client
{

enum my_errors
{
  unsupported_protocol = 1,
  parsing_error,
  cannot_finish_connection
};

class my_error_category_impl : public boost::system::error_category
{
public:
  virtual const char* name() const;
  virtual std::string message(int e) const;
};

const boost::system::error_category& get_my_error_category();
//static const boost::system::error_category& my_error_category = get_my_error_category();

boost::system::error_code make_error_code(my_errors e);

} // namespace http_client

namespace boost
{
namespace system
{
template<> struct is_error_code_enum<http_client::my_errors>
{
  BOOST_STATIC_CONSTANT(bool, value = true);
};
} // namespace system
} // namespace boost

#endif // HTTP_TOY_ERROR_CODE_HPP
